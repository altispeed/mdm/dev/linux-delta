---------------------------------------------------------------------------
-- Copyright (C) 2020 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------
-- Add distro Project Trident.
INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
    'df9b0bbd-0105-4f15-8246-eca6138df7ff',
    'Project Trident',
    'Project Trident is a desktop-focused operating system based on Void Linux. It uses the ' ||
    'Lumina desktop as well as a number of self-developed utilities to provide an easy-to-use ' ||
    'system that both beginners and advanced system administrators can feel comfortable running ' ||
    '24⁄7. Project Trident utilizes the ZFS filesystem to provide snapshots, rollback, as well ' ||
    'as user home directory encryption for data security and integrity.',
    'https://project-trident.org/',
    'https://project-trident.org/',
    'Workstation,Server,IoT,Cloud',
    'project-trident-h.png',
    'project-trident-v.png',
    '#333333'
);

-- Add distro MX Linux.
INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
    '9b40f755-08ec-451a-8177-598a1855a195',
    'MX Linux',
    'MX Linux is a cooperative venture between the antiX and former MEPIS communities, using ' ||
    'the best tools and talents from each distro. It is a midweight OS designed to combine an ' ||
    'elegant and efficient desktop with simple configuration, high stability, solid performance ' ||
    'and medium-sized footprint.',
    'https://mxlinux.org/',
    'https://mxlinux.org/',
    'Workstation,Server,IoT,Cloud',
    'mx-linux-h.svg',
    'mx-linux-v.svg',
    '#000000'
);

-- Add distro Artix.
INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
    '258535ba-07b5-4055-b749-eb00c3a2cbc1',
    'Artix',
    'Artix is a rolling-release distribution based on Arch Linux that uses OpenRC, runit or s6 ' ||
    'init instead of systemd. Artix Linux has its own package repositories but, as a ' ||
    'pacman-based distribution, can use packages from Arch Linux repositories or any other ' ||
    'derivative distribution, even packages explicitly depending on systemd. The Arch User ' ||
    'Repository (AUR) can also be used.',
    'https://en.wikipedia.org/wiki/Artix_Linux',
    'https://artixlinux.org/',
    'Workstation,Server,IoT,Cloud',
    'artix-h.svg',
    'artix-v.svg',
    '#ffffff'
);
---------------------------------------------------------------------------
-- Copyright (C) 2019 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------

ALTER TABLE ratings alter column rate_overall set default 0;
ALTER TABLE ratings alter column rate_server set default 0;
ALTER TABLE ratings alter column rate_workstation set default 0;
ALTER TABLE ratings alter column rate_iot set default 0;
UPDATE ratings SET rate_overall = 0 WHERE rate_overall = 1;
UPDATE ratings SET rate_workstation = 0 WHERE rate_workstation = 1;
UPDATE ratings SET rate_server = 0 WHERE rate_server = 1;
UPDATE ratings SET rate_iot = 0 WHERE rate_iot = 1;
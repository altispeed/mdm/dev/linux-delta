---------------------------------------------------------------------------
-- Copyright (C) 2019 KhronoSync Digital Solutions LLC
--
-- This program is free software: you can redistribute it and/or modify
-- it under the terms of the GNU General Public License as published by
-- the Free Software Foundation, either version 3 of the License, or
-- any later version.
--
-- This program is distributed in the hope that it will be useful,
-- but WITHOUT ANY WARRANTY; without even the implied warranty of
-- MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
-- GNU General Public License for more details.
--
-- You should have received a copy of the GNU General Public License
-- along with this program.  If not, see <https://www.gnu.org/licenses/>
---------------------------------------------------------------------------

INSERT INTO distros (id, name, description, description_source, homepage, tags, logo_image_h, logo_image_v, logo_bg_color)
VALUES
(
  '939c57ce-109a-46b1-81c6-8453411f9a64',
  'Pop!_OS',
  'Pop!_OS is a Linux distribution developed by System76 based on Ubuntu ' ||
  'by Canonical, using the GNOME Desktop Environment. It is intended for ' ||
  'use by "developers, makers, and computer science professionals". ' ||
  'Pop!_OS provides full disk encryption by default as well as streamlined '||
  'window management, workspaces, and keyboard shortcuts for navigation.',
  'https://en.wikipedia.org/wiki/System76#Pop.21_OS',
  'https://system76.com/pop',
  'Workstation,Server,IoT,Cloud',
  'pop_os-horizontal.png',
  'pop_os-vertical.png',
  '#ffffff'
)
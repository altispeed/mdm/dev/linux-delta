package conf

/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
  "encoding/json"
  "io/ioutil"
  "log"
)

type Config struct {
  Admins  map[string]string `json:"admins"`
  DB        dbConfig        `json:"db"`
  Recaptcha RecaptchaConfig `json:"recaptcha"`
}

type dbConfig struct {
  Address   string `json:"address"`
  Username  string `json:"username"`
  Password  string `json:"password"`
}
func (d *dbConfig) Addr() string {
  return d.Address
}
func (d *dbConfig) User() string {
  return d.Username
}
func (d *dbConfig) Pass() string {
  return d.Password
}

type RecaptchaConfig struct {
  SiteKey   string `json:"siteKey"`
  SecretKey  string `json:"secretKey"`
  Threshold  float32 `json:"threshold"`
}

func LoadConfig(path string) Config {
  file, err := ioutil.ReadFile(path)
  if err != nil {
    log.Fatal("Config File Missing. ", err)
  }

  var config Config
  err = json.Unmarshal(file, &config)
  if err != nil {
    log.Fatal("Config Parse Error: ", err)
  }

  return config
}
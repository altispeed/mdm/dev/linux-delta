package routes

/**
 * Copyright (C) 2020 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"github.com/gorilla/mux"
	"gitlab.com/altispeed/linux-delta/internal/app/distros"
	"net/http"
)

const PathReviewHome = "/reviews"
const PathReviewHomeSort = PathReviewHome + "/sort"
const HomeSortAlphabetical = "alphabetical"

type ReviewHomePage struct {
	Session *SessionModel
	Distros []distros.DistroModelView
	Sort string
}

func initializeReviewHomeRoutes(router *mux.Router) {
	router.HandleFunc(PathReviewHome, reviewHomeHandler)
	router.HandleFunc(PathReviewHomeSort, reviewHomeSortHandler).Methods(http.MethodPost)
}

func reviewHomeHandler(w http.ResponseWriter, r *http.Request) {
	allDistros, err := distros.GetAll(HomeSortAlphabetical)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	p := &ReviewHomePage {
		Session: getSession(r),
		Distros: allDistros,
		Sort: HomeSortAlphabetical,
	}

	renderReviewHomeTemplate(w, p)
}

func reviewHomeSortHandler(w http.ResponseWriter, r *http.Request) {
	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	sortOrder := r.FormValue("distro-sort")

	allDistros, err := distros.GetAll(sortOrder)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	p := &ReviewHomePage {
		Session: getSession(r),
		Distros: allDistros,
		Sort: sortOrder,
	}

	renderReviewHomeTemplate(w, p)
}

func renderReviewHomeTemplate(w http.ResponseWriter, page *ReviewHomePage) {
	err := templates.ExecuteTemplate(w, "reviews_home_page.html", page)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
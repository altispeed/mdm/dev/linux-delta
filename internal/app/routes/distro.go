package routes

/**
 * Copyright (C) 2020 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/altispeed/linux-delta/internal/app/conf"
	"gitlab.com/altispeed/linux-delta/internal/app/distros"
	"gitlab.com/altispeed/linux-delta/internal/app/ratings"
	"io/ioutil"
	"net/http"
	"net/url"
	"strconv"
)

const PathDistro = "/distro/{distroName:[a-zA-Z0-9\\-_! ]+}"
const PathDistroRatingSort = PathDistro + "/sort"
const DistroSortRecent = "recent"
const PathRating = "/rating/{ratingId:[a-fA-F0-9\\-]+}"
const PathRatingDelete = PathRating + "/delete"
const PathRatingUseful = PathRating + "/upvote"

type DistroPage struct {
	Session *SessionModel
	Distro *distros.DistroModel
	RatingsInfo []ratings.RatingsInfoModel
	Ratings []ratings.RatingModel
	Sort string
	SiteKey string
}

type RecaptchaResponse struct {
	Success            bool     `json:"success"`
	ChallengeTimestamp string   `json:"challenge_ts"`
	Hostname           string   `json:"hostname"`
	ErrorCodes         []string `json:"error-codes"`
	Score 						 float32	`json:"score"`
}

var siteKey string
var secretKey string
var threshold float32

func initializeDistroRoutes(router *mux.Router, recaptchaConf conf.RecaptchaConfig) {
	siteKey = recaptchaConf.SiteKey
	secretKey = recaptchaConf.SecretKey
	threshold = recaptchaConf.Threshold

	router.HandleFunc(PathDistro, ratingPageHandler).Methods(http.MethodGet)
	router.HandleFunc(PathDistro, ratingFormHandler).Methods(http.MethodPost)
	router.HandleFunc(PathDistro+PathRatingDelete, ratingDeleteHandler)
	router.HandleFunc(PathDistro+PathRatingUseful, ratingUsefulHandler)
	router.HandleFunc(PathDistroRatingSort, ratingSortHandler).
		Methods(http.MethodPost)
}

func ratingPageHandler(w http.ResponseWriter, r *http.Request) {
	distroName := mux.Vars(r)["distroName"]

	p, err := createDistroPage(distroName, r)

	if  err != nil {
		http.NotFound(w, r)
		return
	}

	renderDistroTemplate(w, p)
}

func ratingFormHandler(w http.ResponseWriter, r *http.Request) {
	distroName := mux.Vars(r)["distroName"]
	if distroName == "" {
		http.NotFound(w, r)
		return
	}

	redirectTarget := fmt.Sprintf("/distro/%s", distroName)

	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	success, err := verifyRating(r)
	if !success {
		http.Error(w, "Failed reCAPTCHA", http.StatusBadRequest)
		return
	}
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	id, _ := uuid.NewV4()
	distroId, _ := uuid.FromString(r.FormValue("distroId"))
	username := r.FormValue("name")
	rateOverall, _ := strconv.ParseFloat(r.FormValue("overall"), 64)
	rateWorkstation, _ := strconv.ParseFloat(r.FormValue("workstation"), 64)
	rateServer, _ := strconv.ParseFloat(r.FormValue("server"), 64)
	rateIoT, _ := strconv.ParseFloat(r.FormValue("iot"), 64)
	comment := r.FormValue("comment")

	rating := ratings.RatingModel{
		Id: id,
		DistroId: distroId,
		Username: username,
		RateOverall: rateOverall,
		RateWorkstation: rateWorkstation,
		RateServer: rateServer,
		RateIoT: rateIoT,
		Comment: comment,
	}

	// Don't save rating if nothing is rated.
	if rating.RateIoT != 0 ||
		rating.RateOverall != 0 ||
		rating.RateServer != 0 ||
		rating.RateWorkstation != 0 {
		_, _ = ratings.Save(rating)
	}

	http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func ratingDeleteHandler(w http.ResponseWriter, r *http.Request) {
	sess := getSession(r)
	if sess.Username == "" {
		http.Error(
			w,
			"Unauthorized attempt to delete rating.",
			http.StatusUnauthorized,
		)
		return
	}

	distroName := mux.Vars(r)["distroName"]
	if distroName == "" {
		http.NotFound(w, r)
		return
	}

	redirectTarget := fmt.Sprintf("/distro/%s", distroName)

	ratingId, err := uuid.FromString(mux.Vars(r)["ratingId"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	_, err = ratings.DeleteById(ratingId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func ratingUsefulHandler(w http.ResponseWriter, r *http.Request) {
	distroName := mux.Vars(r)["distroName"]
	if distroName == "" {
		http.NotFound(w, r)
		return
	}

	redirectTarget := fmt.Sprintf("/distro/%s", distroName)

	ratingId, err := uuid.FromString(mux.Vars(r)["ratingId"])
	if err != nil {
		http.NotFound(w, r)
		return
	}

	_, err = ratings.MarkUseful(ratingId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	http.Redirect(w, r, redirectTarget, http.StatusFound)
}

func ratingSortHandler(w http.ResponseWriter, r *http.Request) {
	distroName := mux.Vars(r)["distroName"]
	if distroName == "" {
		http.NotFound(w, r)
		return
	}

	if err := r.ParseForm(); err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	distroId, _ := uuid.FromString(r.FormValue("distroId"))
	sortOrder := r.FormValue("rating-sort")

	distro, err := distros.GetById(distroId)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	ratingsInfo, err := ratings.CalcRatingsInfo(distro.Id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	distroRatings, err := ratings.GetByDistroId(distro.Id, sortOrder)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	p := &DistroPage {
		Session: getSession(r),
		Distro: distro,
		RatingsInfo: ratingsInfo,
		Ratings: distroRatings,
		Sort: sortOrder,
		SiteKey: siteKey,
	}

	renderDistroTemplate(w, p)
}

func createDistroPage(name string, r *http.Request) (*DistroPage, error) {
	distro, err := distros.GetByName(name)
	if err != nil {
		return nil, err
	}

	ratingsInfo, err := ratings.CalcRatingsInfo(distro.Id)
	if err != nil {
		return nil, err
	}

	distroRatings, err := ratings.GetByDistroId(distro.Id, "")
	if err != nil {
		return nil, err
	}

	return &DistroPage {
		Session:     getSession(r),
		Distro:      distro,
		RatingsInfo: ratingsInfo,
		Ratings:     distroRatings,
		Sort:        DistroSortRecent,
		SiteKey: 		 siteKey,
	}, nil
}

func renderDistroTemplate(w http.ResponseWriter, page *DistroPage) {
	err := templates.ExecuteTemplate(w, "distro_page.html", page)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func verifyRating(r *http.Request) (bool, error) {
	if secretKey == "" {
		return true, nil
	}

	var recapResponse string
	recapResponse = r.FormValue("g-recaptcha-response")
	resp, err := http.PostForm(
		"https://www.google.com/recaptcha/api/siteverify",
		url.Values{
			"secret": {secretKey},
			"response": {recapResponse},
		})
	if err != nil {
		return false, err
	}

	defer resp.Body.Close()
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return false, err
	}

	var rr RecaptchaResponse
	err = json.Unmarshal(body, &rr)
	if err != nil {
		return false, err
	}

	return rr.Success == true && rr.Score >= threshold, nil
}
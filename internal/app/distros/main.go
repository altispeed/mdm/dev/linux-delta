/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */
package distros

import (
	"errors"
	"github.com/go-pg/pg"
	uuid "github.com/satori/go.uuid"
	"gitlab.com/altispeed/linux-delta/internal/app/ratings"
	"sort"
)

type DistroModel struct {
	tableName 	struct{} `sql:"distros"`
	Id 					uuid.UUID `sql:"id,type:uuid,pk"`
	Name 				string `sql:"name,type:varchar(100),unique"`
	Description string `sql:"description"`
	DescSource 	string `sql:"description_source"`
	Homepage 		string `sql:"homepage,type:varchar(200)"`
	Tags 				string `sql:"tags,type:varchar(400)"`
	LogoImageH 	string `sql:"logo_image_h,type:varchar(200)"`
	LogoImageV 	string `sql:"logo_image_v,type:varchar(200)"`
	LogoBgColor string `sql:"logo_bg_color,type:varchar(7)"`
}

type DistroModelView struct {
	Id 					uuid.UUID
	Name 				string
	Description string
	DescSource 	string
	Homepage 		string
	Tags 				string
	LogoImageH 	string
	LogoImageV 	string
	LogoBgColor string
	RatingInfoDesktop ratings.RatingsInfoModel
	RatingInfoServer ratings.RatingsInfoModel
	RatingInfoIoT ratings.RatingsInfoModel
	RatingInfoOverall ratings.RatingsInfoModel
}

func dmToDmv(dm DistroModel) (DistroModelView, error) {
	infoList, err := ratings.CalcRatingsInfo(dm.Id)
	if err != nil {
		return DistroModelView{}, err
	}

	dmv := DistroModelView{
		Id:                dm.Id,
		Name:              dm.Name,
		Description:       dm.Description,
		DescSource:        dm.DescSource,
		Homepage:          dm.Homepage,
		Tags:              dm.Tags,
		LogoImageH:        dm.LogoImageH,
		LogoImageV:        dm.LogoImageV,
		LogoBgColor:       dm.LogoBgColor,
		RatingInfoDesktop: ratings.RatingsInfoModel{},
		RatingInfoServer:  ratings.RatingsInfoModel{},
		RatingInfoIoT:     ratings.RatingsInfoModel{},
		RatingInfoOverall: ratings.RatingsInfoModel{},
	}

	for _, info := range infoList {
		switch info.Heading {
		case "Workstation":
			dmv.RatingInfoDesktop = info
			break
		case "Server":
			dmv.RatingInfoServer = info
			break
		case "IoT":
			dmv.RatingInfoIoT = info
			break
		case "Overall":
			dmv.RatingInfoOverall = info
			break
		default:
			return DistroModelView{}, errors.New("Unknown rating type: " + info.Heading)
		}
	}

	return dmv, nil
}

var dr DistroRepository

func Init(db *pg.DB) {
	dr = DistroRepository{ db: db }
}

func Save(model DistroModel) (*DistroModel, error) {
	// Check required fields
	err := isValid(&model)
	if err != nil {
		return nil, err
	}

	// Add new id
	id, err := uuid.NewV4()
	if err != nil {
		return nil, err
	}
	model.Id = id

	// Save to database
	result, err := dr.Create(&model)
	if err != nil {
		return nil, err
	}

	// Return result
	return result, nil
}

func GetAll(sortOrder string) ([]DistroModelView, error) {
	distros, err := dr.Read()
	if err != nil {
		return nil, err
	}

	dmvs := make([]DistroModelView, len(distros))

	for index, dm := range distros {
		dmv, err := dmToDmv(dm)
		if err != nil {
			return nil, err
		}

		dmvs[index] = dmv
	}

	switch sortOrder {
	case "alphabetical":
		return dmvs, nil
	case "top-desktop":
		sort.Slice(dmvs, func(i, j int) bool {
			iAvg := dmvs[i].RatingInfoDesktop.Avg
			jAvg := dmvs[j].RatingInfoDesktop.Avg
			return iAvg > jAvg
		})

		return dmvs, nil
	case "most-desktop":
		sort.Slice(dmvs, func(i, j int) bool {
			iTotal := dmvs[i].RatingInfoDesktop.Total
			jTotal:= dmvs[j].RatingInfoDesktop.Total
			return iTotal > jTotal
		})

		return dmvs, nil
	case "top-server":
		sort.Slice(dmvs, func(i, j int) bool {
			iAvg := dmvs[i].RatingInfoServer.Avg
			jAvg := dmvs[j].RatingInfoServer.Avg
			return iAvg > jAvg
		})

		return dmvs, nil
	case "most-server":
		sort.Slice(dmvs, func(i, j int) bool {
			iTotal := dmvs[i].RatingInfoServer.Total
			jTotal:= dmvs[j].RatingInfoServer.Total
			return iTotal > jTotal
		})

		return dmvs, nil
	case "top-iot":
		sort.Slice(dmvs, func(i, j int) bool {
			iAvg := dmvs[i].RatingInfoIoT.Avg
			jAvg := dmvs[j].RatingInfoIoT.Avg
			return iAvg > jAvg
		})
		return dmvs, nil
	case "most-iot":
		sort.Slice(dmvs, func(i, j int) bool {
			iTotal := dmvs[i].RatingInfoIoT.Total
			jTotal:= dmvs[j].RatingInfoIoT.Total
			return iTotal > jTotal
		})

		return dmvs, nil
	case "top-overall":
		sort.Slice(dmvs, func(i, j int) bool {
			iAvg := dmvs[i].RatingInfoOverall.Avg
			jAvg := dmvs[j].RatingInfoOverall.Avg
			return iAvg > jAvg
		})

		return dmvs, nil
	case "most-overall":
		sort.Slice(dmvs, func(i, j int) bool {
			iTotal := dmvs[i].RatingInfoOverall.Total
			jTotal:= dmvs[j].RatingInfoOverall.Total
			return iTotal > jTotal
		})

		return dmvs, nil
	default:
		return dmvs, nil
	}
}

func GetById(id uuid.UUID) (*DistroModel, error) {
	result, err := dr.ReadById(id)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func GetByName(name string) (*DistroModel, error) {
	result, err := dr.ReadByName(name)
	if err != nil {
		return nil, err
	}

	return result, nil
}

func isValid(model *DistroModel) error {
  // TODO: flesh out
	return nil
}

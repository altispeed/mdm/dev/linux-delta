package ratings

/**
 * Copyright (C) 2019 KhronoSync Digital Solutions LLC
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>
 */

import (
  "github.com/go-pg/pg"
  uuid "github.com/satori/go.uuid"
  "time"
)

type RatingModel struct {
  tableName       struct{}  `sql:"ratings"`
  Id              uuid.UUID `sql:"id,type:uuid,pk"`
  DistroId        uuid.UUID `sql:"distro_id,type:uuid,fk:distros(id)"`
  Username        string    `sql:"username,type:varchar(100)"`
  RateOverall     float64   `sql:"rate_overall,default:0"`
  RateWorkstation float64   `sql:"rate_workstation,default:0"`
  RateServer      float64   `sql:"rate_server,default:0"`
  RateIoT         float64   `sql:"rate_iot,default:0"`
  Comment         string    `sql:"comment,type:varchar(10000)"`
  Upvotes         int64     `sql:"upvotes,default:0"`
  Created         time.Time `sql:"created,default:now()"`
}

type RatingsInfoModel struct {
  Heading       string
  Total         int64
  Total5Star    int64
  Total4Star    int64
  Total3Star    int64
  Total2Star    int64
  Total1Star    int64
  Avg           float64
  Percent5Star  float64
  Percent4Star  float64
  Percent3Star  float64
  Percent2Star  float64
  Percent1Star  float64
}

const SortMostUseful = "upvotes DESC"
const SortMostRecent = "created DESC"

var rr RatingRepository

func Init(db *pg.DB) {
  rr = RatingRepository{ db: db }
}

func Save(model RatingModel) (*RatingModel, error) {
  // Check required fields
  err := isValid(&model)
  if err != nil {
    return nil, err
  }

  // Add new id
  id, err := uuid.NewV4()
  if err != nil {
    return nil, err
  }
  model.Id = id

  // Save to database
  result, err := rr.Create(&model)
  if err != nil {
    return nil, err
  }

  // Return result
  return result, nil
}

func GetAll() ([]RatingModel, error) {
  result, err := rr.Read()
  if err != nil {
    return nil, err
  }

  return result, nil
}

func GetByDistroId(distroId uuid.UUID, sortOrder string) ([]RatingModel, error) {
  var so string

  switch sortOrder {
  case "recent":
    so = SortMostRecent
    break
  case "useful":
    so = SortMostUseful
    break
  default:
    so = SortMostRecent
  }

  results, err := rr.ReadByDistroId(distroId, so)
  if err != nil {
    return nil, err
  }

  return results, nil
}

func GetById(id uuid.UUID) (*RatingModel, error) {
  result, err := rr.ReadById(id)
  if err != nil {
    return nil, err
  }

  return result, nil
}

func DeleteById(id uuid.UUID) (*RatingModel, error) {
  result, err := rr.DeleteById(id)
  if err != nil {
    return nil, err
  }

  return result, nil
}

func MarkUseful(id uuid.UUID) (*RatingModel, error) {
  result, err := rr.MarkUseful(id)
  if err != nil {
    return nil, err
  }

  return result, nil
}

func CalcRatingsInfo(distroId uuid.UUID) ([]RatingsInfoModel, error) {
  ratings, err := rr.ReadByDistroId(distroId, "")
  if err != nil {
    return nil, err
  }

  return aggregateRatingInfo(ratings), nil
}

func isValid(model *RatingModel) error {
  // TODO: flesh out
  return nil
}

func aggregateRatingInfo(ratings []RatingModel) []RatingsInfoModel {
  ov := RatingsInfoModel{
    Heading: "Overall",
    Total5Star: 0,
    Total4Star: 0,
    Total3Star: 0,
    Total2Star: 0,
    Total1Star: 0,
    Total: 0,
  }

  wo := RatingsInfoModel{
    Heading: "Workstation",
    Total5Star: 0,
    Total4Star: 0,
    Total3Star: 0,
    Total2Star: 0,
    Total1Star: 0,
    Total: 0,
  }

  se := RatingsInfoModel{
    Heading: "Server",
    Total5Star: 0,
    Total4Star: 0,
    Total3Star: 0,
    Total2Star: 0,
    Total1Star: 0,
    Total: 0,
  }

  io := RatingsInfoModel{
    Heading: "IoT",
    Total5Star: 0,
    Total4Star: 0,
    Total3Star: 0,
    Total2Star: 0,
    Total1Star: 0,
    Total: 0,
  }

  total := len(ratings)
  for i:=0;i<total;i++ {
    rating := ratings[i]
    countRating(rating.RateOverall, &ov)
    countRating(rating.RateWorkstation, &wo)
    countRating(rating.RateServer, &se)
    countRating(rating.RateIoT, &io)
  }

  calcPercentages(&ov)
  calcPercentages(&wo)
  calcPercentages(&se)
  calcPercentages(&io)

  return []RatingsInfoModel{ov, wo, se, io}
}

func countRating(rating float64, rim *RatingsInfoModel) {
  if rating == 5.0 {
    rim.Total5Star += 1
    rim.Total += 1
  } else if rating == 4.0 {
    rim.Total4Star += 1
    rim.Total += 1
  } else if rating == 3.0 {
    rim.Total3Star += 1
    rim.Total += 1
  } else if rating == 2.0 {
    rim.Total2Star += 1
    rim.Total += 1
  } else if rating == 1.0 {
    rim.Total1Star += 1
    rim.Total += 1
  }
}

func calcPercentages(rim *RatingsInfoModel) {
   total := rim.Total

  if total == 0 {
    rim.Avg = 0
  } else {
    rim.Avg = float64(
      (rim.Total5Star * 5) +
      (rim.Total4Star * 4) +
      (rim.Total3Star * 3) +
      (rim.Total2Star * 2) +
      rim.Total1Star) / float64(rim.Total)
  }

  rim.Percent5Star = calcPercent(rim.Total5Star, total)
  rim.Percent4Star = calcPercent(rim.Total4Star, total)
  rim.Percent3Star = calcPercent(rim.Total3Star, total)
  rim.Percent2Star = calcPercent(rim.Total2Star, total)
  rim.Percent1Star = calcPercent(rim.Total1Star, total)
}

func calcPercent(starRating int64, total int64) float64 {
  if total == 0 {
    return 0.00
  }

  return float64(starRating) / float64(total) * 100.0
}